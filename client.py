###############################
# Cliente: envia al servidor un mensaje, imprime la respuesta del servidor y termina
#
# se ejecuta con: python3 cliente.py 
###############################

import socket
import sys

# Crear socket TCP/IP
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Conectar el socker al puerto donde el servidor esta escuchando
server_address = ('localhost', 10000)

expression = 'conectandose al host  %s puerto %s' % server_address
print(expression, file=sys.stderr)

sock.connect(server_address)

try:

    # Enviar dato, codificado en bytes
    message = 'Hola soy un cliente.'.encode()

    expression = 'enviando mensaje: "%s"' % message 
    print(expression, file=sys.stderr)


    sock.sendall(message)

    # Esperando por la respuesta
    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = sock.recv(8)
        amount_received += len(data)
        expression = 'received "%s"' % data 
        print(expression, file=sys.stderr)

finally:
    expression = 'closing socket' 
    print(expression, file=sys.stderr)

    sock.close()

