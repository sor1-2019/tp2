###############################
# Servidor: escucha conexiones, es un servidor tipo eco, 
# los clientes envian mensajes al servidor y el servidor devuelve al cliente el mismo mensaje 
#
# se ejecuta con: python3 server.py 
###############################
import socket
import sys

# Crear un socket TCP
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


# Ligar el socket al puerto
server_address = ('localhost', 10000)

expression = 'Iniciando server en host %s puerto %s' % server_address
print(expression, file=sys.stderr)


sock.bind(server_address)

# Escuchar por conexiones
sock.listen(1)

while True:
    # Esperar por una conexion
    expression = 'listo para aceptar una conexion'
    print(expression, file=sys.stderr)


    connection, client_address = sock.accept()
    try:
        expression = 'conexion entrante de', client_address
        print(expression, file=sys.stderr)

        # Recibe los datos en partes chicas y los retransmite
        while True:
            #leer una cantidad de bytes acotadaa
            cant_bytes=4 
            data = connection.recv(cant_bytes)
            expression = 'recibido "%s"' % data
            print(expression, file=sys.stderr)

            if data:
                expression = 'enviando de nuevo al cliente'
                print(expression, file=sys.stderr)

                connection.sendall(data)
            else:
                expression = 'no hay mas datos', client_address
                print(expression, file=sys.stderr)

                break
    finally:
        # Cerrar la conexion
        connection.close()


#puede verificar si el servidor esta escuchando con la siguiente instruccion
# netstat -an

